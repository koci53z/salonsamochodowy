package interfaces;

public interface PrinterMVC {
    interface Controller {

        void println(Object object);

    }
}
