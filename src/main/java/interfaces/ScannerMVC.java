package interfaces;

public interface ScannerMVC {
    interface Controller {

        int nextInt();

        int pickOption();
    }
}
