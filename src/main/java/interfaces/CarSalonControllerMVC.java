package interfaces;

public interface CarSalonControllerMVC {
    interface Controller{
        void configure();
    }
}
