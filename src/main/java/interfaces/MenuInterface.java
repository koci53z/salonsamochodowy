package interfaces;

public interface MenuInterface {

    void goToColorPick();
    void goToFuelTypePick();
    void goToBrandPick();
    void goToBodyTypePick();
    void goToUpholsteryPick();
}
