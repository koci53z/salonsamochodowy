package interfaces;

import model.Car;

public interface CarCreatorControllerMVC {
    interface Controller {
        void pickColor();
        void pickFuelType();
        void pickBodyType();
        void pickBrand();
        void pickUpholstery();
        Car returnCar();
    }
}
