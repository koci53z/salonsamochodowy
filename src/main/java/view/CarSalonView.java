package view;

import controller.CarCreatorController;
import controller.CarSalonController;
import controller.PrinterController;
import controller.ScannerController;
import interfaces.CarCreatorControllerMVC;
import interfaces.CarSalonControllerMVC;
import interfaces.PrinterMVC;
import model.Car;
import model.Person;
import interfaces.ScannerMVC;

public class CarSalonView {

    private ScannerMVC.Controller scannerMVC = new ScannerController();
    private Person person = new Person();
    private CarCreatorControllerMVC.Controller carCreatorControllerMVC;
    private CarSalonControllerMVC.Controller carSalonController;
    private PrinterMVC.Controller printer = new PrinterController();



    public void start() {
        carCreatorControllerMVC = new CarCreatorController(scannerMVC, new Car(), person, printer);
        person.createWallet();
        carSalonController = new CarSalonController(carCreatorControllerMVC, scannerMVC, person, printer);
        carSalonController.configure();
    }
}

