package model;

import controller.PrinterController;
import interfaces.MenuInterface;
import interfaces.PrinterMVC;


public enum BodyType {
    SEDAN(0),
    NONE (0),
    PICKUP(2000),
    HATCHBACK(1000),
    KOMBI(1000);

    private int price;
    private PrinterMVC.Controller printer = new PrinterController();


    BodyType(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
    public void checkPickedBodyType(Person person, Car car, MenuInterface menuInterface) {
        if (person.getCurrentWallet() + car.getBodyType().getPrice() >= this.getPrice()) {
            person.addMoney(car.getBodyType().getPrice());
            car.setBodyType(this);
            person.subtractMoney(this.getPrice());
        } else {
            printer.println("Not enough money!");
            menuInterface.goToBodyTypePick();
        }
    }
}

