package model;

import controller.PrinterController;
import controller.ScannerController;
import interfaces.PrinterMVC;
import interfaces.ScannerMVC;

public class Person {

    private int currentWallet;
    private ScannerMVC.Controller scannerController = new ScannerController();
    private PrinterMVC.Controller printer = new PrinterController();


    public int getCurrentWallet() {
        return currentWallet;
    }

    public void subtractMoney (int money) {
        this.currentWallet -= money;
    }

    public void addMoney (int money) {
        this.currentWallet += money;
    }

    public void createWallet () {
        printer.println("Podaj ilość gotówki");
        this.currentWallet += scannerController.nextInt();
    }

}
