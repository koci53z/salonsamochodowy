package model;

import controller.PrinterController;
import interfaces.MenuInterface;
import interfaces.PrinterMVC;

public enum Color {
    WHITE(0),
    NONE(0),
    BLACK(0),
    SILVER(0),
    GOLD(1000),
    RED(1000);
    
    private int price;
    private PrinterMVC.Controller printer = new PrinterController();


    Color(int price) {
        this.price = price;
    }
    public int getPrice() {
        return price;
    }

    public void checkPickedColor(Person person, Car car, MenuInterface menuInterface) {
        if (person.getCurrentWallet() + car.getColor().getPrice() >= this.getPrice()) {
            person.addMoney(car.getColor().getPrice());
            car.setColor(this);
            person.subtractMoney(this.getPrice());

        } else {
            printer.println("Not enough money!");
            menuInterface.goToColorPick();
        }
    }
}