package model;

import controller.PrinterController;
import interfaces.MenuInterface;
import interfaces.PrinterMVC;

public enum Upholstery {

    WELUR(0),
    NONE (0),
    SKORA(1000),
    SKORApikowana(2000);

    private int price;
    private PrinterMVC.Controller printer = new PrinterController();


    Upholstery(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void checkPickedUpholstery(Person person, Car car, MenuInterface menuInterface) {
        if (person.getCurrentWallet() + car.getUpholstery().getPrice() >= this.getPrice()) {
            person.addMoney(car.getUpholstery().getPrice());
            car.setUpholstery(this);
            person.subtractMoney(this.getPrice());
        } else {
            printer.println("Not enough money!");
            menuInterface.goToUpholsteryPick();
        }
    }
}
