package model;

public class Car {

    private Color color;
    private FuelType fuelType;
    private BodyType bodyType;
    private Brand brand;
    private Upholstery upholstery;


    public Car() {
        this.color = Color.NONE;
        this.fuelType = FuelType.NONE;
        this.bodyType = BodyType.NONE;
        this.brand = Brand.NONE;
        this.upholstery = Upholstery.NONE;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public BodyType getBodyType() {
        return bodyType;
    }

    public void setBodyType(BodyType bodyType) {
        this.bodyType = bodyType;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Upholstery getUpholstery() {
        return upholstery;
    }

    public void setUpholstery(Upholstery upholstery) {
        this.upholstery = upholstery;
    }

    @Override
    public String toString() {
        return "WYBRANY SAMOCHÓD: " +
                "KOLOR: " + color +
                ", PALIWO: " + fuelType +
                ", NADWOZIE: " + bodyType +
                ", MARKA: " + brand +
                ", TAPICERKA: " + upholstery +
                '.';
    }
}
