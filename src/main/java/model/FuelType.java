package model;

import controller.PrinterController;
import interfaces.MenuInterface;
import interfaces.PrinterMVC;

public enum FuelType {

    BENZYNA(0),
    NONE(0),
    DIESEL(10000),
    HYBRYDA(15000);

    private int price;
    private PrinterMVC.Controller printer = new PrinterController();


    FuelType(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void checkPickedFuelType(Person person, Car car, MenuInterface menuInterface) {
        if (person.getCurrentWallet() + car.getFuelType().getPrice() >= this.getPrice()) {
            person.addMoney(car.getFuelType().getPrice());
            car.setFuelType(this);
            person.subtractMoney(this.getPrice());
        } else {
            printer.println("Not enough money!");
            menuInterface.goToFuelTypePick();
        }
    }
}