package model;

import controller.PrinterController;
import interfaces.MenuInterface;
import interfaces.PrinterMVC;

public enum Brand {

    VOLVO(10000),
    NONE (0),
    AUDI(15000),
    LEXUS(20000),
    CHRYSLER(25000),
    BENTLEY(30000);

    private int price;
    private PrinterMVC.Controller printer = new PrinterController();


    Brand(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;

    }
    public void checkPickedBrand(Person person, Car car, MenuInterface menuInterface) {
        if (person.getCurrentWallet() + car.getBrand().getPrice() >= this.getPrice()) {
            person.addMoney(car.getBrand().getPrice());
            car.setBrand(this);
            person.subtractMoney(this.getPrice());
        } else {
            printer.println("Not enough money!");
            menuInterface.goToBrandPick();
        }
    }
}

