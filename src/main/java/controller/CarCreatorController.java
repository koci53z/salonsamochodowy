package controller;


import interfaces.MenuInterface;
import interfaces.PrinterMVC;
import model.*;
import interfaces.CarCreatorControllerMVC;
import interfaces.ScannerMVC;


public class CarCreatorController implements CarCreatorControllerMVC.Controller, MenuInterface {

    private Car car;
    private ScannerMVC.Controller scannerController;
    private Person person;
    private PrinterMVC.Controller printer;

    public CarCreatorController(ScannerMVC.Controller scannerController, Car car, Person person, PrinterMVC.Controller printer) {
        this.car = car;
        this.scannerController = scannerController;
        this.person = person;
        this.printer = printer;
    }

    public void pickColor() {
        printer.println("Wybierz kolor");
        int option = scannerController.pickOption();

        Color pickedColor;
        pickedColor = Color.NONE;
        switch (option) {
            case 1:
                pickedColor = Color.WHITE;
                break;
            case 2:
                pickedColor = Color.BLACK;
                break;
            case 3:
                pickedColor = Color.SILVER;
                break;
            case 4:
                pickedColor = Color.GOLD;
                break;
            case 5:
                pickedColor = Color.RED;
                break;
            default:
                pickColor();
                break;
        }

        if (pickedColor != Color.NONE) {
            pickedColor.checkPickedColor(person, car, this);
        }
    }


    public void pickFuelType() {
        printer.println("Wybierz paliwo");
        int option = scannerController.pickOption();
        FuelType pickFuelType;
        pickFuelType = FuelType.NONE;

        switch (option) {
            case 1:
                pickFuelType = FuelType.BENZYNA;
                break;
            case 2:
                pickFuelType = FuelType.DIESEL;
                break;
            case 3:
                pickFuelType = FuelType.HYBRYDA;
                break;
            default:
                pickFuelType();
                break;
        }
        if (pickFuelType != FuelType.NONE) {
            pickFuelType.checkPickedFuelType(person, car, this);
        }
    }


    public void pickBrand() {
        printer.println("Wybierz marke");
        int option = scannerController.pickOption();
        Brand pickBrand;
        pickBrand = Brand.NONE;

        switch (option) {
            case 1:
                pickBrand = Brand.VOLVO;
                break;
            case 2:
                pickBrand = Brand.AUDI;
                break;
            case 3:
                pickBrand = Brand.LEXUS;
                break;
            case 4:
                pickBrand = Brand.CHRYSLER;
                break;
            case 5:
                pickBrand = Brand.BENTLEY;
                break;
            default:
                pickBrand();
                break;
        }
        if (pickBrand != Brand.NONE) {
            pickBrand.checkPickedBrand(person, car, this);
        }
    }

    public void pickBodyType() {
        printer.println("Wybierz nadwozie");
        int option = scannerController.pickOption();
        BodyType pickBodyType;
        pickBodyType = BodyType.NONE;

        switch (option) {
            case 1:
                pickBodyType = BodyType.SEDAN;
                break;
            case 2:
                pickBodyType = BodyType.PICKUP;
                break;
            case 3:
                pickBodyType = BodyType.HATCHBACK;
                break;
            case 4:
                pickBodyType = BodyType.KOMBI;
                break;
            default:
                pickBodyType();
                break;
        }
        if (pickBodyType != BodyType.NONE) {
            pickBodyType.checkPickedBodyType(person, car, this);
        }
    }

    public void pickUpholstery() {
        printer.println("Wybierz tapicerke");
        int option = scannerController.pickOption();
        Upholstery pickUpholstery;
        pickUpholstery = Upholstery.NONE;

        switch (option) {
            case 1:
                pickUpholstery = Upholstery.WELUR;
                break;
            case 2:
                pickUpholstery = Upholstery.SKORA;
                break;
            case 3:
                pickUpholstery = Upholstery.SKORApikowana;
                break;
            default:
                pickUpholstery();
                break;
        }
        if (pickUpholstery != Upholstery.NONE) {
            pickUpholstery.checkPickedUpholstery(person, car, this);
        }
    }

    public Car returnCar (){
        return car;
    }

    @Override
    public void goToColorPick() {
        pickColor();
    }

    @Override
    public void goToFuelTypePick() {
        pickFuelType();
    }

    @Override
    public void goToBrandPick() {
        pickBrand();
    }

    @Override
    public void goToBodyTypePick() {
        pickBodyType();
    }

    @Override
    public void goToUpholsteryPick() {
        pickUpholstery();
    }
}
