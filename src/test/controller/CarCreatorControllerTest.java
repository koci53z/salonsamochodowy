package controller;

import interfaces.PrinterMVC;
import model.BodyType;
import model.Car;
import model.Color;
import model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import interfaces.ScannerMVC;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class CarCreatorControllerTest {

    Person person = new Person();
    Person personPoor = new Person();
    Car car = new Car();
    PrinterMVC.Controller printer = mock(PrinterMVC.Controller.class) ;
    ScannerMVC.Controller controller = mock(ScannerMVC.Controller.class);
    CarCreatorController tested = new CarCreatorController(controller, car, person, printer);
    CarCreatorController testedPoor = new CarCreatorController(controller, car, personPoor, printer);

    @BeforeEach
    public void setUpWallet(){
        person.addMoney(10000000);
        personPoor.addMoney(100);
    }

    @Test
    public void pickingColorTest1(){
        when(controller.pickOption()).thenReturn(1);
        tested.pickColor();
        assertEquals(car.getColor(), Color.WHITE);
    }

    @Test
    public void pickingColorTest2(){
        when(controller.pickOption()).thenReturn(2);
        tested.pickColor();
        assertEquals(car.getColor(), Color.BLACK);
    }

    @Test
    public void pickingColorTest3(){
        when(controller.pickOption()).thenReturn(3);
        tested.pickColor();
        assertEquals(car.getColor(), Color.SILVER);
    }

    @Test
    public void pickingColorTest4(){
        when(controller.pickOption()).thenReturn(4);
        tested.pickColor();
        assertEquals(car.getColor(), Color.GOLD);
    }

    @Test
    public void pickingColorTest5(){
        when(controller.pickOption()).thenReturn(5);
        tested.pickColor();
        assertEquals(car.getColor(), Color.RED);
    }


    @Test
    public void walletTest1(){
        when(controller.pickOption()).thenReturn(4);
        tested.pickColor();
        assertEquals(person.getCurrentWallet(), 10000000 - Color.GOLD.getPrice() );
    }

    @Test
    public void walletTest2(){
        when(controller.pickOption()).thenReturn(5);
        tested.pickColor();
        assertEquals(person.getCurrentWallet(), 10000000 - Color.RED.getPrice() );
    }


    @Test
    public void walletPoorTest1(){
        when(controller.pickOption()).thenReturn(4,1);
        testedPoor.pickColor();
        verify(printer,times(2)).println(anyString());
        verify(printer,atMost(1)).println("Not enough money!");

    }

    @Test
    public void walletPoorTest2(){
        when(controller.pickOption()).thenReturn(5,1);
        testedPoor.pickColor();
        verify(printer,atMost(1)).println("Not enough money!");
    }


    @Test
    public void walletPoorTest3(){
        when(controller.pickOption()).thenReturn(4,1);
        testedPoor.pickColor();
        verify(printer,atLeastOnce()).println("Wybierz kolor");
    }

    @Test
    public void walletPoorTest4(){
        when(controller.pickOption()).thenReturn(5,1);
        testedPoor.pickColor();
        verify(printer,atLeastOnce()).println("Wybierz kolor");
    }

    @Test
    public void walletPoorTest5(){
        when(controller.pickOption()).thenReturn(4,1);
        testedPoor.pickColor();
        verify(printer,times(2)).println(anyString());
    }
    @Test
    public void pickBodyTypeTest1() {
        when(controller.pickOption()).thenReturn(1);
        tested.pickBodyType();
        assertEquals(car.getBodyType(), BodyType.SEDAN);

}
    @Test
    public void pickBodyTypeTest2() {
        when(controller.pickOption()).thenReturn(2);
        tested.pickBodyType();
        assertEquals(car.getBodyType(), BodyType.PICKUP);
    }
    @Test
    public void pickBodyTypeTest3() {
        when(controller.pickOption()).thenReturn(3);
        tested.pickBodyType();
        assertEquals(car.getBodyType(), BodyType.HATCHBACK);
    }
    @Test
    public void pickBodyTypeTest4() {
        when(controller.pickOption()).thenReturn(4);
        tested.pickBodyType();
        assertEquals(car.getBodyType(), BodyType.KOMBI);
    }
   }