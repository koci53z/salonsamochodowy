package controller;

import interfaces.CarCreatorControllerMVC;
import interfaces.CarSalonControllerMVC;
import interfaces.PrinterMVC;
import interfaces.ScannerMVC;
import model.Car;
import model.Person;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.mockito.Mockito.*;

public class CarSalonControllerTest {

    Person person = new Person();
    Car car = new Car();
    PrinterMVC.Controller printerController = mock(PrinterMVC.Controller.class) ;
    ScannerMVC.Controller scannerController = mock(ScannerMVC.Controller.class);
    CarCreatorControllerMVC.Controller carCreaterController = mock(CarCreatorControllerMVC.Controller.class);
    CarSalonControllerMVC.Controller tested = new CarSalonController(carCreaterController,scannerController,person,printerController);

    @BeforeEach
    public void setUpWallet(){
        person.addMoney(10000000);
    }

    @Test
    public void menuPrintest(){
        when(scannerController.pickOption()).thenReturn(6);
        tested.configure();
        verify(printerController,times(9)).println(anyString());
    }

    @Test
    public void menuOptionTest1(){
        when(scannerController.pickOption()).thenReturn(1,0).thenReturn(6);
        tested.configure();
        verify(carCreaterController, times(1)).pickColor();
    }

    @Test
    public void menuOptionTest2(){
        when(scannerController.pickOption()).thenReturn(2,0).thenReturn(6);
        tested.configure();
        verify(carCreaterController, times(1)).pickFuelType();
    }

    @Test
    public void menuOptionTest3(){
        when(scannerController.pickOption()).thenReturn(3,0).thenReturn(6);
        tested.configure();
        verify(carCreaterController, times(1)).pickBrand();
    }

    @Test
    public void menuOptionTest4(){
        when(scannerController.pickOption()).thenReturn(4,0).thenReturn(6);
        tested.configure();
        verify(carCreaterController, times(1)).pickBodyType();
    }

    @Test
    public void menuOptionTest5(){
        when(scannerController.pickOption()).thenReturn(5,0).thenReturn(6);
        tested.configure();
        verify(carCreaterController, times(1)).pickUpholstery();
    }

    @Test
    public void menuOptionTest6(){
        when(scannerController.pickOption()).thenReturn(6,0);
        tested.configure();
        verifyZeroInteractions(carCreaterController);

    }

    @Test
    public void menuOptionTest7(){
        when(scannerController.pickOption()).thenReturn(7,0).thenReturn(6);
        tested.configure();
        verify(printerController, times(1)).println(person.getCurrentWallet());

    }

    @Test
    public void menuOptionTest8(){
        when(scannerController.pickOption()).thenReturn(8,0).thenReturn(6);
        tested.configure();
        verify(printerController, times(1)).println(carCreaterController.returnCar());
        doReturn(car).when(carCreaterController).returnCar();
    }

    @Test
    public void menuOptionTest9(){
        when(scannerController.pickOption()).thenReturn(9,0).thenReturn(6);
        tested.configure();
        verifyZeroInteractions(carCreaterController);
    }
}