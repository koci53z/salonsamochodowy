package model;

import interfaces.MenuInterface;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class UpholsteryTest {


    Person person = new Person();
    Person personPoor = new Person();
    Car car = new Car();
    MenuInterface menuInterface = mock(MenuInterface.class);

    @BeforeEach
    public void setUpWalletPoor() {
        personPoor.addMoney(0);
        person.addMoney(1000000);
    }


    @Test
    public void testSkora() {
        Upholstery.SKORA.checkPickedUpholstery(personPoor, car, menuInterface);
        verify(menuInterface).goToUpholsteryPick();
    }

    @Test
    public void testSkora2() {
        Upholstery.SKORA.checkPickedUpholstery(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Upholstery.SKORA.getPrice());
        assertEquals(car.getUpholstery(), Upholstery.SKORA);
    }

    @Test
    public void testWelur() {
        Upholstery.WELUR.checkPickedUpholstery(personPoor, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Upholstery.WELUR.getPrice());
        assertEquals(car.getUpholstery(), Upholstery.WELUR);

    }

    @Test
    public void testWelur2() {
        Upholstery.WELUR.checkPickedUpholstery(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Upholstery.WELUR.getPrice());
        assertEquals(car.getUpholstery(), Upholstery.WELUR);
    }

    @Test
    public void testSKORApikowana() {
        Upholstery.SKORApikowana.checkPickedUpholstery(personPoor, car, menuInterface);
        verify(menuInterface).goToUpholsteryPick();
    }

    @Test
    public void testSKORApikowana2() {
        Upholstery.SKORApikowana.checkPickedUpholstery(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Upholstery.SKORApikowana.getPrice());
        assertEquals(car.getUpholstery(), Upholstery.SKORApikowana);
    }


}