package model;

import interfaces.MenuInterface;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class BrandTest {

    Person person = new Person();
    Person personPoor = new Person();
    Car car = new Car();
    MenuInterface menuInterface = mock(MenuInterface.class);

    @BeforeEach
    public void setUpWalletPoor() {
        personPoor.addMoney(0);
        person.addMoney(1000000);
    }


    @Test
    public void testVolvo() {
        Brand.VOLVO.checkPickedBrand(personPoor, car, menuInterface);
        verify(menuInterface).goToBrandPick();
    }

    @Test
    public void testVolvo2() {
        Brand.VOLVO.checkPickedBrand(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Brand.VOLVO.getPrice());
        assertEquals(car.getBrand(), Brand.VOLVO);
    }

    @Test
    public void testAudi() {
        Brand.AUDI.checkPickedBrand(personPoor, car, menuInterface);
        verify(menuInterface).goToBrandPick();


    }

    @Test
    public void testAudi2() {
        Brand.AUDI.checkPickedBrand(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Brand.AUDI.getPrice());
        assertEquals(car.getBrand(), Brand.AUDI);
    }

    @Test
    public void testChryslerp() {
        Brand.CHRYSLER.checkPickedBrand(personPoor, car, menuInterface);
        verify(menuInterface).goToBrandPick();
    }

    @Test
    public void testChrysler2() {
        Brand.CHRYSLER.checkPickedBrand(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Brand.CHRYSLER.getPrice());
        assertEquals(car.getBrand(), Brand.CHRYSLER);
    }

    @Test
    public void testLexus() {
        Brand.LEXUS.checkPickedBrand(personPoor, car, menuInterface);
        verify(menuInterface).goToBrandPick();
    }

    @Test
    public void testLexus2() {
        Brand.LEXUS.checkPickedBrand(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Brand.LEXUS.getPrice());
        assertEquals(car.getBrand(), Brand.LEXUS);
    }
    @Test
    public void testBentley() {
        Brand.BENTLEY.checkPickedBrand(personPoor, car, menuInterface);
        verify(menuInterface).goToBrandPick();
    }

    @Test
    public void testBentley2() {
        Brand.BENTLEY.checkPickedBrand(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Brand.BENTLEY.getPrice());
        assertEquals(car.getBrand(), Brand.BENTLEY);
    }

}