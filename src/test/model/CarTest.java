package model;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class CarTest {

    Car car = new Car();

    @Test
    public void bodyTest0() {
        assertEquals(car.getBodyType(), BodyType.NONE);
    }

    @Test
    public void bodyTest1() {
        car.setBodyType(BodyType.HATCHBACK);
        assertEquals(car.getBodyType(), BodyType.HATCHBACK);
    }

    @Test
    public void bodyTest2() {
        car.setBodyType(BodyType.KOMBI);
        assertEquals(car.getBodyType(), BodyType.KOMBI);
    }

    @Test
    public void bodyTest3() {
        car.setBodyType(BodyType.SEDAN);
        assertEquals(car.getBodyType(), BodyType.SEDAN);
    }

    @Test
    public void bodyTest4() {
        car.setBodyType(BodyType.PICKUP);
        assertEquals(car.getBodyType(), BodyType.PICKUP);
    }

    @Test
    public void brandTest0() {
        assertEquals(car.getBrand(), Brand.NONE);
    }

    @Test
    public void brandTest1() {
        car.setBrand(Brand.AUDI);
        assertEquals(car.getBrand(), Brand.AUDI);
    }

    @Test
    public void brandTest2() {
        car.setBrand(Brand.BENTLEY);
        assertEquals(car.getBrand(), Brand.BENTLEY);
    }

    @Test
    public void brandTest3() {
        car.setBrand(Brand.CHRYSLER);
        assertEquals(car.getBrand(), Brand.CHRYSLER);
    }

    @Test
    public void brandTest4() {
        car.setBrand(Brand.LEXUS);
        assertEquals(car.getBrand(), Brand.LEXUS);
    }

    @Test
    public void brandTest5() {
        car.setBrand(Brand.VOLVO);
        assertEquals(car.getBrand(), Brand.VOLVO);
    }

    @Test
    public void colorTest0() {
        assertEquals(car.getColor(), Color.NONE);
    }

    @Test
    public void colorTest1() {
        car.setColor(Color.GOLD);
        assertEquals(car.getColor(), Color.GOLD);
    }

    @Test
    public void colorTest2() {
        car.setColor(Color.RED);
        assertEquals(car.getColor(), Color.RED);
    }

    @Test
    public void colorTest3() {
        car.setColor(Color.BLACK);
        assertEquals(car.getColor(), Color.BLACK);
    }

    @Test
    public void colorTest4() {
        car.setColor(Color.SILVER);
        assertEquals(car.getColor(), Color.SILVER);
    }

    @Test
    public void colorTest5() {
        car.setColor(Color.WHITE);
        assertEquals(car.getColor(), Color.WHITE);
    }

    @Test
    public void fuelTest0() {
        assertEquals(car.getFuelType(), FuelType.NONE);
    }

    @Test
    public void fuelTest1() {
        car.setFuelType(FuelType.BENZYNA);
        assertEquals(car.getFuelType(), FuelType.BENZYNA);
    }

    @Test
    public void fuelTest2() {
        car.setFuelType(FuelType.DIESEL);
        assertEquals(car.getFuelType(), FuelType.DIESEL);
    }

    @Test
    public void fuelTest3() {
        car.setFuelType(FuelType.HYBRYDA);
        assertEquals(car.getFuelType(), FuelType.HYBRYDA);
    }


    @Test
    public void upholsteryTest0() {
        assertEquals(car.getUpholstery(), Upholstery.NONE);
    }

    @Test
    public void upholsteryTest1() {
        car.setUpholstery(Upholstery.SKORA);
        assertEquals(car.getUpholstery(), Upholstery.SKORA);

    }

    @Test
    public void upholsteryTest2() {
        car.setUpholstery(Upholstery.WELUR);
        assertEquals(car.getUpholstery(), Upholstery.WELUR);

    }

    @Test
    public void upholsteryTest3() {
        car.setUpholstery(Upholstery.SKORApikowana);
        assertEquals(car.getUpholstery(), Upholstery.SKORApikowana);

    }
}