package model;

import interfaces.MenuInterface;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ColorTest {

    Person person = new Person();
    Person personPoor = new Person();
    Car car = new Car();
    MenuInterface menuInterface = mock(MenuInterface.class);

    @BeforeEach
    public void setUpWalletPoor() {
        personPoor.addMoney(0);
        person.addMoney(1000000);
    }


    @Test
    public void testWhite() {
        Color.WHITE.checkPickedColor(personPoor, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Color.WHITE.getPrice());
        assertEquals(car.getColor(), Color.WHITE);
    }

    @Test
    public void testWhite2() {
        Color.WHITE.checkPickedColor(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Color.WHITE.getPrice());
        assertEquals(car.getColor(), Color.WHITE);
    }

    @Test
    public void testBlack() {
        Color.BLACK.checkPickedColor(personPoor, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Color.BLACK.getPrice());
        assertEquals(car.getColor(), Color.BLACK);
    }

    @Test
    public void testBlack2() {
        Color.BLACK.checkPickedColor(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Color.BLACK.getPrice());
        assertEquals(car.getColor(), Color.BLACK);
    }

    @Test
    public void testSilver() {
        Color.SILVER.checkPickedColor(personPoor, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Color.SILVER.getPrice());
        assertEquals(car.getColor(), Color.SILVER);
    }

    @Test
    public void testSilver2() {
        Color.SILVER.checkPickedColor(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Color.SILVER.getPrice());
        assertEquals(car.getColor(), Color.SILVER);
    }

    @Test
    public void testRed() {
        Color.RED.checkPickedColor(personPoor, car, menuInterface);
        verify(menuInterface).goToColorPick();
    }

    @Test
    public void testRed2() {
        Color.RED.checkPickedColor(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Color.RED.getPrice());
        assertEquals(car.getColor(), Color.RED);
    }
    @Test
    public void testGold() {
        Color.GOLD.checkPickedColor(personPoor, car, menuInterface);
        verify(menuInterface).goToColorPick();
    }

    @Test
    public void testGold2() {
        Color.GOLD.checkPickedColor(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - Color.GOLD.getPrice());
        assertEquals(car.getColor(), Color.GOLD);
    }
    

}