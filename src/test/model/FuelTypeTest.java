package model;

import interfaces.MenuInterface;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class FuelTypeTest {

    Person person = new Person();
    Person personPoor = new Person();
    Car car = new Car();
    MenuInterface menuInterface = mock(MenuInterface.class);

    @BeforeEach
    public void setUpWalletPoor() {
        personPoor.addMoney(0);
        person.addMoney(1000000);
    }


    @Test
    public void testDiesel() {
        FuelType.DIESEL.checkPickedFuelType(personPoor, car, menuInterface);
        verify(menuInterface).goToFuelTypePick();
    }

    @Test
    public void testDiesel2() {
        FuelType.DIESEL.checkPickedFuelType(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - FuelType.DIESEL.getPrice());
        assertEquals(car.getFuelType(), FuelType.DIESEL);
    }

    @Test
    public void testBenzyna() {
        FuelType.BENZYNA.checkPickedFuelType(personPoor, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - FuelType.BENZYNA.getPrice());
        assertEquals(car.getFuelType(), FuelType.BENZYNA);

    }

    @Test
    public void testBenzyna2() {
        FuelType.BENZYNA.checkPickedFuelType(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - FuelType.BENZYNA.getPrice());
        assertEquals(car.getFuelType(), FuelType.BENZYNA);
    }

    @Test
    public void testHybryda() {
        FuelType.HYBRYDA.checkPickedFuelType(personPoor, car, menuInterface);
        verify(menuInterface).goToFuelTypePick();
    }

    @Test
    public void testHybryda2() {
        FuelType.HYBRYDA.checkPickedFuelType(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - FuelType.HYBRYDA.getPrice());
        assertEquals(car.getFuelType(), FuelType.HYBRYDA);
    }   

}