package model;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PersonTest {

    Person person = new Person();

    @Test
    public void personTest1(){
        person.addMoney(10000);
        assertEquals(10000,person.getCurrentWallet());
    }

    @Test
    public void personTest2(){
        person.addMoney(10000);
        person.subtractMoney(5000);
        assertEquals(5000,person.getCurrentWallet());
    }

}