package model;

import controller.CarCreatorController;
import interfaces.MenuInterface;
import interfaces.PrinterMVC;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class BodyTypeTest {

    Person person = new Person();
    Person personPoor = new Person();
    Car car = new Car();
    MenuInterface menuInterface = mock(MenuInterface.class);

    @BeforeEach
    public void setUpWalletPoor() {
        personPoor.addMoney(0);
        person.addMoney(1000000);
    }


    @Test
    public void testKombi() {
        BodyType.KOMBI.checkPickedBodyType(personPoor, car, menuInterface);
        verify(menuInterface).goToBodyTypePick();
    }

    @Test
    public void testKombi2() {
        BodyType.KOMBI.checkPickedBodyType(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - BodyType.KOMBI.getPrice());
        assertEquals(car.getBodyType(), BodyType.KOMBI);
    }

    @Test
    public void testSedan() {
        BodyType.SEDAN.checkPickedBodyType(personPoor, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - BodyType.SEDAN.getPrice());
        assertEquals(car.getBodyType(), BodyType.SEDAN);

    }

    @Test
    public void testSedan2() {
        BodyType.SEDAN.checkPickedBodyType(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - BodyType.SEDAN.getPrice());
        assertEquals(car.getBodyType(), BodyType.SEDAN);
    }

    @Test
    public void testPickup() {
        BodyType.PICKUP.checkPickedBodyType(personPoor, car, menuInterface);
        verify(menuInterface).goToBodyTypePick();
    }

    @Test
    public void testPickup2() {
        BodyType.PICKUP.checkPickedBodyType(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - BodyType.PICKUP.getPrice());
        assertEquals(car.getBodyType(), BodyType.PICKUP);
    }

    @Test
    public void testHatchback() {
        BodyType.HATCHBACK.checkPickedBodyType(personPoor, car, menuInterface);
        verify(menuInterface).goToBodyTypePick();
    }

    @Test
    public void testHatchback2() {
        BodyType.HATCHBACK.checkPickedBodyType(person, car, menuInterface);
        assertEquals(person.getCurrentWallet(), 1000000 - BodyType.HATCHBACK.getPrice());
        assertEquals(car.getBodyType(), BodyType.HATCHBACK);
    }
}